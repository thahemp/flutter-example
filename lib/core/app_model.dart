import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'dart:convert';
import 'package:http/http.dart' as http;
import '../util/date_util.dart';
import './auth_message.dart';
import './get_calendar_items_message.dart';

class AppModel extends Model {
  // URI of AuthDB server
  Uri authURI = Uri.http("127.0.0.1:7343", "/signin");
  // URI of API server
  Uri apiURI = Uri.http("127.0.0.1:7343", "/api");
  // URI for user info changes
  Uri modifyUserURI = Uri.http("127.0.0.1:7343", "/modify-user");
  // URI for calendar requests
  Uri calendarURI = Uri.http("127.0.0.1:7344", "/time");

  // The current API token value
  String apiToken = "uninitialized";
  // Default string for DashBoard status header
  final String defaultStatusMsg = "MC2 HR Dashboard";
  // String to be used for DashBoard status header
  String statusMsg = "";

  // Storage for user data fetched from server
  AuthMessage userData = AuthMessage();
  // Storage for calendar data fetched from server
  GetCalendarItemsMessage calendarData = GetCalendarItemsMessage();

  bool isApiTokenValid() {
    if (apiToken.isEmpty) {
      return false;
    }

    return true;
  }

  setApiToken(String? tok, String status) async {
    apiToken = tok ?? "";
    statusMsg = status;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("AuthToken", apiToken);

    notifyListeners();
  }

  modifyUserResponseHandler(AuthMessage? msg, String status) {
    statusMsg = status;

    if (msg != null) {
      userData = msg;
    } else {
      userData = AuthMessage();
    }

    notifyListeners();
  }

  updateCalendarData(GetCalendarItemsMessage msg) {
    calendarData = msg;

    notifyListeners();
  }

  doHandshake() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    // Go ahead and initialize apiToken so the LoginDialog doesn't flicker
    // on-screen when we don't really need to login.
    apiToken = prefs.getString("AuthToken") ?? "";

    // First thing to do is seend any stored token we already have and see if it
    // is still valid. If valid we get a fresh API token without having to login.
    AuthMessage msg = AuthMessage('handshake', prefs.getString("AuthToken"));

    final response = await http.post(
      authURI,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(msg),
    );

    if (response.statusCode == 200) {
      // Token was good
      AuthMessage resMsg = AuthMessage.fromJson(jsonDecode(response.body));
      // Store our User data
      userData = resMsg;
      setApiToken(resMsg.token, defaultStatusMsg);
    } else if (response.statusCode == 403) {
      userData = AuthMessage();
      setApiToken(null, "Login Required...");
    }
  }

  doLogin(String user, String password) async {
    AuthMessage msg = AuthMessage('login', apiToken, user, password);

    final response = await http.post(
      authURI,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(msg),
    );

    if (response.statusCode == 200) {
      // Login successful
      AuthMessage resMsg = AuthMessage.fromJson(jsonDecode(response.body));
      // Store our User data
      userData = resMsg;
      setApiToken(resMsg.token, defaultStatusMsg);
    } else if (response.statusCode == 403) {
      // Login failed
      userData = AuthMessage();
      setApiToken(null, response.body);
    }
  }

  doLogout() async {
    AuthMessage msg = AuthMessage('logout', apiToken);

    final response = await http.post(
      authURI,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(msg),
    );

    if (response.statusCode == 200) {
      // Logout successful
      AuthMessage resMsg = AuthMessage.fromJson(jsonDecode(response.body));
      // Store our User data
      userData = resMsg;
      setApiToken(resMsg.token, defaultStatusMsg);
    } else if (response.statusCode == 403) {
      // Logout failed. Probably a bad token, which means we're already logged out
      userData = AuthMessage();
      setApiToken(null, response.body);
    }
  }

  createNewUser(String user, String password, String firstName, String lastName, String phone, String email, String authLevel) async {
    // When creating a new user, our apiToken is sent. It is used by the backend to verify that we
    // have the authority to create new users.
    AuthMessage msg = AuthMessage('add', apiToken, user, password, firstName, lastName, phone, email, authLevel);

    final response = await http.post(
      modifyUserURI,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(msg),
    );

    if (response.statusCode == 200) {
      // Login successful
      AuthMessage resMsg = AuthMessage.fromJson(jsonDecode(response.body));
      modifyUserResponseHandler(resMsg, "Successfully added user: " + resMsg.user!);
    } else if (response.statusCode == 403) {
      modifyUserResponseHandler(null, response.body);
    }
  }

  editUserInfo(String user, String? password, String? firstName, String? lastName, String? phone, String? email, String? authLevel) async {
    AuthMessage msg = AuthMessage('edit', apiToken, user, password, firstName, lastName, phone, email, authLevel);

    final response = await http.post(
      modifyUserURI,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(msg),
    );

    if (response.statusCode == 200) {
      // Login successful
      AuthMessage resMsg = AuthMessage.fromJson(jsonDecode(response.body));
      modifyUserResponseHandler(resMsg, "Successfully modified user: " + resMsg.user!);
    } else if (response.statusCode == 403) {
      modifyUserResponseHandler(null, response.body);
    }
  }

  deactivateUser(String user) async {}

  getCalendarItems(DateTime start, DateTime end) async {
    String s = DateUtil.dateToRFC3339String(start);
    String e = DateUtil.dateToRFC3339String(end);

    GetCalendarItemsMessage msg = GetCalendarItemsMessage(apiToken, s, e);

    final response = await http.post(
      calendarURI,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(msg),
    );

    // print('getCalendarItems RESPONSE: ' + response.body);

    if (response.statusCode == 200) {
      // Login successful
      GetCalendarItemsMessage resMsg = GetCalendarItemsMessage.fromJson(jsonDecode(response.body));
      updateCalendarData(resMsg);
    } else {
      print("getCalendarItems() failed: " + response.body);
    }
  }

  AppModel();
}
