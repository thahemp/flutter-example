import 'package:json_annotation/json_annotation.dart';
import '../util/date_util.dart';

part 'auth_message.g.dart';

// Run 'flutter pub run build_runner build'
// in package dir to regenerate auth_message.g.dart
// This must be done every time this file changes

@JsonSerializable()
@RFC3339DateTimeConverter()
class AuthMessage {
  // type: "handshake", "login", "logout"
  @JsonKey(name: 'type')
  String? type;
  @JsonKey(name: 'token')
  String? token;

  // username in AuthDB
  @JsonKey(name: 'user')
  String? user;
  // password in AuthDB - this will be null in the server response
  @JsonKey(name: 'password')
  String? password;

  @JsonKey(name: 'firstName')
  String? firstName;
  @JsonKey(name: 'lastName')
  String? lastName;
  @JsonKey(name: 'phone')
  String? phone;
  @JsonKey(name: 'email')
  String? email;

  // authLevel: "administrator", "manager", "user"
  @JsonKey(name: 'authLevel')
  String? authLevel;
  // subordinates gives list of users whos data is accessible by this user
  @JsonKey(name: 'subordinates')
  List<String>? subordinates;

  AuthMessage(
      [this.type, this.token, this.user, this.password, this.firstName, this.lastName, this.phone, this.email, this.authLevel, this.subordinates]);

  factory AuthMessage.fromJson(Map<String, dynamic> json) => _$AuthMessageFromJson(json);

  Map<String, dynamic> toJson() => _$AuthMessageToJson(this);
}
