// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_message.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AuthMessage _$AuthMessageFromJson(Map<String, dynamic> json) => AuthMessage(
      json['type'] as String?,
      json['token'] as String?,
      json['user'] as String?,
      json['password'] as String?,
      json['firstName'] as String?,
      json['lastName'] as String?,
      json['phone'] as String?,
      json['email'] as String?,
      json['authLevel'] as String?,
      (json['subordinates'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$AuthMessageToJson(AuthMessage instance) =>
    <String, dynamic>{
      'type': instance.type,
      'token': instance.token,
      'user': instance.user,
      'password': instance.password,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'phone': instance.phone,
      'email': instance.email,
      'authLevel': instance.authLevel,
      'subordinates': instance.subordinates,
    };
