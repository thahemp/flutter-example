import 'package:json_annotation/json_annotation.dart';
import '../util/date_util.dart';

part 'get_calendar_items_message.g.dart';

// Run 'flutter pub run build_runner build'
// in package dir to regenerate auth_message.g.dart
// This must be done every time this file changes

@JsonSerializable()
@RFC3339DateTimeConverter()
class GetCalendarItemsMessage {
  @JsonKey(name: 'token')
  String? token;
  @JsonKey(name: 'startDate')
  String? startDate;
  @JsonKey(name: 'endDate')
  String? endDate;
  @JsonKey(name: 'timeItems')
  List<TimeItem>? timeItems;
  @JsonKey(name: 'timeOffItems')
  List<TimeOffItem>? timeOffItems;

  GetCalendarItemsMessage([this.token, this.startDate, this.endDate, this.timeItems, this.timeOffItems]);

  factory GetCalendarItemsMessage.fromJson(Map<String, dynamic> json) => _$GetCalendarItemsMessageFromJson(json);

  Map<String, dynamic> toJson() => _$GetCalendarItemsMessageToJson(this);
}

@JsonSerializable()
@RFC3339DateTimeConverter()
class TimeItem {
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'description')
  String? description;
  @JsonKey(name: 'finalized')
  bool finalized;
  @JsonKey(name: 'username')
  String? username;
  @JsonKey(name: 'firstName')
  String? firstName;
  @JsonKey(name: 'lastName')  
  String? lastName;
  @JsonKey(name: 'startDate')
  String? startDate;
  @JsonKey(name: 'endDate')
  String? endDate;

  TimeItem(this.finalized, [this.name, this.description, this.username, this.firstName, this.lastName, this.startDate, this.endDate]);

  factory TimeItem.fromJson(Map<String, dynamic> json) => _$TimeItemFromJson(json);

  Map<String, dynamic> toJson() => _$TimeItemToJson(this);
}

@JsonSerializable()
@RFC3339DateTimeConverter()
class TimeOffItem {
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'description')
  String? description;
  @JsonKey(name: 'finalized')
  bool finalized;
  @JsonKey(name: 'condition')
  bool condition;
  @JsonKey(name: 'username')
  String? username;
  @JsonKey(name: 'firstName')
  String? firstName;
  @JsonKey(name: 'lastName')
  String? lastName;
  @JsonKey(name: 'startDate')
  String? startDate;
  @JsonKey(name: 'endDate')
  String? endDate;

  TimeOffItem(this.finalized, this.condition, [this.name, this.description, this.username, this.firstName, this.lastName, this.startDate, this.endDate]);

  factory TimeOffItem.fromJson(Map<String, dynamic> json) => _$TimeOffItemFromJson(json);

  Map<String, dynamic> toJson() => _$TimeOffItemToJson(this);
}
