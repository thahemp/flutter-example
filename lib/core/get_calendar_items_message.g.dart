// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_calendar_items_message.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetCalendarItemsMessage _$GetCalendarItemsMessageFromJson(
        Map<String, dynamic> json) =>
    GetCalendarItemsMessage(
      json['token'] as String?,
      json['startDate'] as String?,
      json['endDate'] as String?,
      (json['timeItems'] as List<dynamic>?)
          ?.map((e) => TimeItem.fromJson(e as Map<String, dynamic>))
          .toList(),
      (json['timeOffItems'] as List<dynamic>?)
          ?.map((e) => TimeOffItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GetCalendarItemsMessageToJson(
        GetCalendarItemsMessage instance) =>
    <String, dynamic>{
      'token': instance.token,
      'startDate': instance.startDate,
      'endDate': instance.endDate,
      'timeItems': instance.timeItems,
      'timeOffItems': instance.timeOffItems,
    };

TimeItem _$TimeItemFromJson(Map<String, dynamic> json) => TimeItem(
      json['finalized'] as bool,
      json['name'] as String?,
      json['description'] as String?,
      json['username'] as String?,
      json['firstName'] as String?,
      json['lastName'] as String?,
      json['startDate'] as String?,
      json['endDate'] as String?,
    );

Map<String, dynamic> _$TimeItemToJson(TimeItem instance) => <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'finalized': instance.finalized,
      'username': instance.username,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'startDate': instance.startDate,
      'endDate': instance.endDate,
    };

TimeOffItem _$TimeOffItemFromJson(Map<String, dynamic> json) => TimeOffItem(
      json['finalized'] as bool,
      json['condition'] as bool,
      json['name'] as String?,
      json['description'] as String?,
      json['username'] as String?,
      json['firstName'] as String?,
      json['lastName'] as String?,
      json['startDate'] as String?,
      json['endDate'] as String?,
    );

Map<String, dynamic> _$TimeOffItemToJson(TimeOffItem instance) =>
    <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'finalized': instance.finalized,
      'condition': instance.condition,
      'username': instance.username,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'startDate': instance.startDate,
      'endDate': instance.endDate,
    };
