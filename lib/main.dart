import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import 'core/app_model.dart';
import './routes.dart';

main() async {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final model = AppModel();

    model.doHandshake();

    return ScopedModel<AppModel>(
      model: model,
      child: _buildMaterialApp(model),
    );
  }

  MaterialApp _buildMaterialApp(AppModel model) {
    return MaterialApp(
      title: 'Flutter Example',
      initialRoute: kDashboardRoute.key,
      routes: getAllRoutes(),
      debugShowCheckedModeBanner: false,
    );
  }
}
