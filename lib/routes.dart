import 'dart:core';
import 'package:flutter/widgets.dart';
import './ui/login_dialog.dart';
import './ui/dashboard.dart';

// Define routes
final MapEntry<String, Widget Function(BuildContext)> kDashboardRoute =
    MapEntry('/', (BuildContext context) => const Dashboard());
final MapEntry<String, Widget Function(BuildContext)> kLoginDialogRoute =
    MapEntry('/signin', (BuildContext context) => const LoginDialog());
// final MapEntry<String, Widget Function(BuildContext)> kAddRecipeRoute = MapEntry('/add', (BuildContext context) => AddRecipeScreen());

// Get all available routes
Map<String, Widget Function(BuildContext)> getAllRoutes() {
  return Map.fromEntries([
    kDashboardRoute,
    kLoginDialogRoute,
    // kAddRecipeRoute
  ]);
}
