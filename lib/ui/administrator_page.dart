import 'package:flutter/material.dart';
import './calendar.dart';
import './manage_employee_info_card.dart';

class AdminstratorPage extends StatelessWidget {
  const AdminstratorPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: const [
        Expanded(child: Card(child: Calendar())),
        ManageEmployeeInfoCard(),
      ],
    );
  }
}
