import 'package:flutter/material.dart';
import 'package:mc2_hr_gui/ui/calendar_item_form.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:intl/intl.dart';

import '../core/app_model.dart';
import '../util/date_util.dart';

class Calendar extends StatefulWidget {
  const Calendar({Key? key}) : super(key: key);

  @override
  CalendarState createState() {
    return CalendarState();
  }
}

class CalendarState extends State<Calendar> {
  @override
  void initState() {
    super.initState();
  }

  List<CalendarResource> _resources = <CalendarResource>[];
  // List of CalendarItems contained in a CalendarCell. Used to populate detail view underneath calendar view.
  List<CalendarItem> _selectedCellCalendarItems = <CalendarItem>[];

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<AppModel>(
      builder: (context, child, model) => Column(
        children: [
          /// Calendar
          Expanded(
            flex: 8,
            child: SfCalendar(
              view: CalendarView.month,
              timeZone: "Central Standard Time",
              dataSource: CalendarItemsDataSource(_getDataSource(model), _resources),
              monthViewSettings: const MonthViewSettings(
                appointmentDisplayMode: MonthAppointmentDisplayMode.indicator,
              ),
              showNavigationArrow: true,
              allowDragAndDrop: true,
              initialSelectedDate: DateTime.now(),
              onTap: calendarTapped,
              onViewChanged: calendarViewChanged,
              onSelectionChanged: calendarSelectionChanged,
            )),
          /// Bottom CalendarItem detail pane
          Expanded(
            child: Container(
              color: Colors.black12,
              child: ListView.separated(
                padding: const EdgeInsets.all(2),
                itemCount: _selectedCellCalendarItems.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    padding: const EdgeInsets.all(2),
                    height: 60,
                    color: _selectedCellCalendarItems[index].color,
                    child: ListTile(
                      leading: Column(
                        children: <Widget>[
                          Text(
                            _selectedCellCalendarItems[index].isAllDay ? '' : DateFormat('hh:mm a').format(_selectedCellCalendarItems[index].from),
                            textAlign: TextAlign.center,
                            style: const TextStyle(fontWeight: FontWeight.w600, color: Colors.white, height: 1.7),
                          ),
                          Text(
                            _selectedCellCalendarItems[index].isAllDay ? 'All day' : '',
                            style: const TextStyle(height: 0.5, color: Colors.white),
                          ),
                          Text(
                            _selectedCellCalendarItems[index].isAllDay ? '' : DateFormat('hh:mm a').format(_selectedCellCalendarItems[index].to),
                            textAlign: TextAlign.center,
                            style: const TextStyle(fontWeight: FontWeight.w600, color: Colors.white),
                          ),
                        ],
                      ),
                      trailing: Icon(
                        getIcon(_selectedCellCalendarItems[index].name),
                        size: 30,
                        color: Colors.white,
                      ),
                      title: Text(
                        _selectedCellCalendarItems[index].name,
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.w600, color: Colors.white),
                      ),
                    ));
              },
              separatorBuilder: (BuildContext context, int index) => const Divider(height: 5),
            ),
          )),
        ],
      ),
    );
  }

  // Provide CalendarItem data to the Calendar widget using the data currently stored by the AppModel
  List<CalendarItem> _getDataSource(AppModel model) {
    // The model.cal
    final calItemsMessage = model.calendarData;
    List<CalendarItem> items = [];

    // Loop for each CalendarItem type: timeItem, timeOffItem, holidayItem, eventItem, closedItem, ...
    if (calItemsMessage.timeItems != null) {
      for (var item in calItemsMessage.timeItems!) {
        String subject = (item.lastName ?? "") + ", " + (item.firstName ?? "") + ", " + (item.name ?? "");
        CalendarItem newA = CalendarItem(
          from: DateUtil.dateFromRFC3339String(item.startDate!),
          to: DateUtil.dateFromRFC3339String(item.endDate!),
          isAllDay: false,
          name: subject,
          description: item.description,
        );

        items.add(newA);
      }
    }

    if (calItemsMessage.timeOffItems != null) {
      for (var item in calItemsMessage.timeOffItems!) {
        String subject = (item.lastName ?? "") + ", " + (item.firstName ?? "") + ", " + (item.name ?? "");
        CalendarItem newA = CalendarItem(
          from: DateUtil.dateFromRFC3339String(item.startDate!),
          to: DateUtil.dateFromRFC3339String(item.endDate!),
          isAllDay: false,
          name: subject,
          description: item.description,
        );

        items.add(newA);
      }
    }

    return items;
  }

  // This callback is used to add/edit appointments. It will spawn a CalendarItemForm.
  void calendarTapped(CalendarTapDetails details) {
    switch (details.targetElement) {
      case CalendarElement.calendarCell:
        // Set our appointments array for this cell so that the agenda
        // view ListView has the data it needs to fill out the view
        setState(() {
          _selectedCellCalendarItems = details.appointments!.cast<CalendarItem>();
        });
        break;
      case CalendarElement.appointment:
        showDialog(
            context: context,
            builder: (context) {
              return CalendarItemDialog(details.appointments!.first.name, details.appointments!.first.from, details.appointments!.first.to);
            });
        break;
      default:
        break;
    }
  }

  // This callback is the perfect place to trigger our app model to pull new
  // CalendarItems. It is triggered when this view loads and when months are changed.
  void calendarViewChanged(ViewChangedDetails details) {
    ScopedModel.of<AppModel>(context).getCalendarItems(details.visibleDates.first, details.visibleDates.last);
  }

  // Callback triggered any time the selected CalendarCell changes. Similar to calendarTapped callback,
  // but this one is called when the view loads and the initial CalendarCell selection occurs.
  void calendarSelectionChanged(CalendarSelectionDetails details) {
    
  }

  // Icon definitions for various CalendarItem types
  IconData getIcon(String subject) {
    if (subject == 'Planning') {
      return Icons.subject;
    } else if (subject == 'Development Meeting   New York, U.S.A') {
      return Icons.people;
    } else if (subject == 'Support - Web Meeting   Dubai, UAE') {
      return Icons.settings;
    } else if (subject == 'Project Plan Meeting   Kuala Lumpur, Malaysia') {
      return Icons.check_circle_outline;
    } else if (subject == 'Retrospective') {
      return Icons.people_outline;
    } else if (subject == 'Project Release Meeting   Istanbul, Turkey') {
      return Icons.people_outline;
    } else if (subject == 'Customer Meeting   Tokyo, Japan') {
      return Icons.settings_phone;
    } else if (subject == 'Release Meeting') {
      return Icons.view_day;
    } else {
      return Icons.beach_access;
    }
  }
}

//
// Support classes for the calendar widget
//

// CalendarItem is our 
class CalendarItem {
  CalendarItem({required this.name, this.description, required this.from, required this.to, this.color = Colors.lightBlue, this.isAllDay = false});

  String name;
  String? description;
  DateTime from;
  DateTime to;
  Color color;
  bool isAllDay;
}

// Bridge class that maps our CalendarItem type to the SFCalendar Appointment type
class CalendarItemsDataSource extends CalendarDataSource {
  CalendarItemsDataSource(List<CalendarItem> source, List<CalendarResource> sourceResource) {
    appointments = source;
    resources = sourceResource;
  }

  @override
  DateTime getStartTime(int index) {
    return appointments![index].from;
  }

  @override
  DateTime getEndTime(int index) {
    return appointments![index].to;
  }

  @override
  String getSubject(int index) {
    return appointments![index].name;
  }

  @override
  Color getColor(int index) {
    return appointments![index].color;
  }

  @override
  bool isAllDay(int index) {
    return appointments![index].isAllDay;
  }
}
