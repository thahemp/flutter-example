import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:mc2_hr_gui/core/app_model.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class CalendarItemDialog extends StatefulWidget {
  final String _type;
  final DateTime _ogstart;
  final DateTime _ogend;
  TimeOfDay? _editedStartTime;
  TimeOfDay? _editedEndTime;

  CalendarItemDialog(String type, DateTime? start, DateTime? end, {Key? key})
      : _type = type,
        _ogstart = start ?? DateTime.now(),
        _ogend = end ?? DateTime.now(),
        super(key: key) {
    _editedStartTime = TimeOfDay.fromDateTime(_ogstart.toLocal());
    _editedEndTime = TimeOfDay.fromDateTime(_ogend.toLocal());
  }

  @override
  CalendarItemDialogState createState() {
    return CalendarItemDialogState();
  }
}

class CalendarItemDialogState extends State<CalendarItemDialog> {
  @override
  void initState() {
    super.initState();
  }

  // Show TimePicker dialog when user clicks one of the time buttons
  void _selectTime(String which) async {
    TimeOfDay initTime;

    if (which == "start") {
      initTime = widget._editedStartTime!;
    } else {
      initTime = widget._editedEndTime!;
    }

    final TimeOfDay? newTime = await showTimePicker(
      context: context,
      initialTime: initTime,
    );

    if (newTime != null) {
      setState(() {
        if (which == "start") {
          widget._editedStartTime = newTime;
        } else {
          widget._editedEndTime = newTime;
        }
      });
    }
  }

  // Sanitize selected DateTime values and send to backend
  void _submit(PickerDateRange value) {
    PickerDateRange range = value as PickerDateRange;
    DateTime editedStartDate, editedEndDate;

    editedStartDate = DateTime(range.startDate!.year, range.startDate!.month, range.startDate!.day, widget._editedStartTime!.hour,
        widget._editedStartTime!.minute);

    if (range.endDate != null) {
      editedEndDate = DateTime(
        range.endDate!.year, range.endDate!.month, range.endDate!.day, 
        widget._editedEndTime!.hour, widget._editedEndTime!.minute
      );
    } else {
      editedEndDate = DateTime(
        editedStartDate.year, editedStartDate.month, editedStartDate.day,
        widget._editedEndTime!.hour, widget._editedEndTime!.minute
      );
    }

    print(editedStartDate.toLocal().toString() + " to " + editedEndDate.toLocal().toString());
  }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey createdf above.
    return Form(
      child: AlertDialog(
        title: Text(widget._type),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                Expanded(
                  child: ElevatedButton(
                    onPressed: () => _selectTime("start"),
                    style: ElevatedButton.styleFrom(elevation: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text("Start"), 
                        Expanded(child: Text(widget._editedStartTime!.format(context), textAlign: TextAlign.center)),
                        const Icon(Icons.access_time),
                      ],
                    ),
                  ),
                ),
                const Padding(padding: EdgeInsets.all(2.0)),
                Expanded(
                  child: ElevatedButton(
                    onPressed: () => _selectTime("end"),
                    style: ElevatedButton.styleFrom(elevation: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text("End"), 
                        Expanded(child: Text(widget._editedEndTime!.format(context), textAlign: TextAlign.center)),
                        const Icon(Icons.access_time),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
                width: 400,
                height: 300,
                child: SfDateRangePicker(
                  selectionMode: DateRangePickerSelectionMode.range,
                  showActionButtons: true,
                  initialDisplayDate: widget._ogstart,
                  initialSelectedRange: PickerDateRange(widget._ogstart, widget._ogend),
                  // enablePastDates: false,
                  onSubmit: (value) => _submit,
                  onCancel: () {
                    Navigator.pop(context);
                  },
                )),
          ],
        ),
      ),
    );
  }
}
