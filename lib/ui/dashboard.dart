import 'package:flutter/material.dart';
import 'package:mc2_hr_gui/ui/administrator_page.dart';
import 'package:mc2_hr_gui/ui/login_dialog.dart';
import 'package:mc2_hr_gui/ui/manager_page.dart';
import 'package:mc2_hr_gui/ui/user_page.dart';
import 'package:scoped_model/scoped_model.dart';

import '../core/app_model.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  Widget _buildBody(BuildContext context, AppModel model) {
    // If we don't have a valid API token, then we need to build the dashboard
    // differently. The LoginDialog is a modal window and can't be created
    // during the dashboard build function. addPostFrameCallback let's us
    // show it after the dashboard is built. We don't know what rights the
    // user should have until they are logged in, so we return an empty
    // container until the LoginDialog is satisfied.
    if (!model.isApiTokenValid()) {
      WidgetsBinding.instance!.addPostFrameCallback((_) {
        showDialog(
            context: context,
            barrierDismissible: false,
            builder: (context) { return const LoginDialog(); });
      });
      return Container();
    } else {
      
      switch (model.userData.authLevel) {
        case "administrator":
          return const AdminstratorPage();
        case "manager":
          return const ManagerPage();
        case "user":
          return const UserPage();
      }
      return Container();
    }
  }

  // Only show logout button if API token is valid
  Widget _logoutButton(BuildContext context, AppModel model) {
    if (model.isApiTokenValid()) {
      return IconButton(
          onPressed: () {
            model.doLogout();
          },
          icon: const Icon(Icons.logout),
          tooltip: "Logout",);
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<AppModel>(
      builder: (context, child, model) => Scaffold(
        backgroundColor: Colors.grey,
        appBar: AppBar(
          backgroundColor: Colors.blueGrey,
          centerTitle: true,
          title: Text(model.statusMsg),
          actions: [
            _logoutButton(context, model),
          ],
        ),
        body: _buildBody(context, model),
      ),
    );
  }
}
