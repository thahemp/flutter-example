import 'package:flutter/material.dart';
import 'package:mc2_hr_gui/core/app_model.dart';
import 'package:scoped_model/scoped_model.dart';

class LoginDialog extends StatefulWidget {
  const LoginDialog({Key? key}) : super(key: key);

  @override
  LoginDialogState createState() {
    return LoginDialogState();
  }
}

class LoginDialogState extends State<LoginDialog> {
  final _formKey = GlobalKey<FormState>();
  final _userFocusNode = FocusNode();
  final _passFocusNode = FocusNode();
  // If username text field needs focus, then don't give it to the password field
  bool _userNeedsFocus = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    TextEditingController _userController = TextEditingController();
    TextEditingController _passController = TextEditingController();

    // Handler for submit button and "Enter" button presses in text fields
    void handleFieldSubmitted() {
      if (_formKey.currentState!.validate()) {
        // Initiate login via AppModel call
        ScopedModel.of<AppModel>(context).doLogin(_userController.text, _passController.text);

        Navigator.pop(context);
      }
    }

    // Build a Form widget using the _formKey createdf above.
    return Form(
      key: _formKey,
      child: AlertDialog(
        title: const Text("MC2 HR Login"),
        content: Column(mainAxisSize: MainAxisSize.min, children: [
          // Username field
          TextFormField(
            controller: _userController,
            focusNode: _userFocusNode,
            autofocus: true,
            decoration: const InputDecoration(
              icon: Icon(Icons.person),
              labelText: 'Username',
            ),
            validator: (value) {
              if (value == null || value.isEmpty) {
                _userFocusNode.requestFocus();
                return 'Username is required';
              }
              return null;
            },
            onEditingComplete: handleFieldSubmitted,
          ),
          // Password field
          TextFormField(
            controller: _passController,
            focusNode: _passFocusNode,
            obscureText: true,
            decoration: const InputDecoration(
              icon: Icon(Icons.password),
              labelText: 'Password',
            ),
            validator: (value) {
              if (value == null || value.isEmpty) {
                if (_userController.text.isNotEmpty) {
                  _passFocusNode.requestFocus();
                }                  
                return 'Password is required';
              }
              return null;
            },
            onEditingComplete: handleFieldSubmitted,
          ),
        ]),
        // AlertDialog submit button
        actions: <Widget>[
          TextButton(
            child: const Text('Submit'),
            onPressed: handleFieldSubmitted,
          ),
        ],
      ),
    );
  }
}
