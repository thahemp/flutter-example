import 'package:flutter/material.dart';
import 'package:mc2_hr_gui/core/types.dart';
import 'package:scoped_model/scoped_model.dart';
import '../core/app_model.dart';
import './user_info_form.dart';

class ManageEmployeeInfoCard extends StatelessWidget {
  const ManageEmployeeInfoCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextStyle style = const TextStyle(fontSize: 18);

    void handleButtonPress(BuildContext context, AppModel model, ModifyUserButtonFuntion b) {
      showDialog(
          context: context,
          builder: (context) {
            return ScopedModel<AppModel>(model: model, child: UserInfoForm(b));
          });
    }

    return ScopedModelDescendant<AppModel>(
        builder: (context, child, model) => Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                /// 'Manage Employee Information' Card ///
                SizedBox(
                  height: 150,
                  width: double.infinity,
                  child: Card(
                    color: Colors.transparent,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const SizedBox(height: 10),
                        const Text("Manage User Information", style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
                        const SizedBox(height: 10),
                        const Divider(thickness: 2.0, color: Colors.black38, indent: 10.0, endIndent: 10.0),
                        const SizedBox(height: 10),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(minimumSize: const Size(200, 40), primary: Colors.orange),
                              onPressed: () => handleButtonPress(context, model, ModifyUserButtonFuntion.edit),
                              child: Text("EDIT USER", style: style),
                            ),
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(minimumSize: const Size(200, 40), primary: Colors.green),
                              onPressed: () => handleButtonPress(context, model, ModifyUserButtonFuntion.add),
                              child: Text("ADD USER", style: style),
                            ),
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(minimumSize: const Size(200, 40), primary: Colors.deepOrange),
                              onPressed: () => handleButtonPress(context, model, ModifyUserButtonFuntion.deactivate),
                              child: Text("DEACTIVATE USER", style: style),
                            ),
                          ],
                        ),
                        const SizedBox(height: 20),
                      ],
                    ),
                  ),
                ),
              ],
            ));
  }
}
