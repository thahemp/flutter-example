import 'package:flutter/material.dart';
import 'package:mc2_hr_gui/core/app_model.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';

import '../core/types.dart';

class UserInfoForm extends StatefulWidget {
  final ModifyUserButtonFuntion formType;

  const UserInfoForm(this.formType, {Key? key}) : super(key: key);

  @override
  UserInfoFormState createState() {
    return UserInfoFormState();
  }
}

class UserInfoFormState extends State<UserInfoForm> {
  final _formKey = GlobalKey<FormState>();

  // Text field storage
  final TextEditingController _userController = TextEditingController();
  final TextEditingController _passController = TextEditingController();
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();

  PhoneNumber _phoneNumber = PhoneNumber(isoCode: "US");

  // Radio button storage
  int authLevelRadioId = 3;
  String authLevelString = 'user';

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Text titleStr;
    switch (widget.formType) {
      case ModifyUserButtonFuntion.edit:
        titleStr = const Text("Edit User");
        break;
      case ModifyUserButtonFuntion.add:
        titleStr = const Text("Add User");
        break;
      case ModifyUserButtonFuntion.deactivate:
        titleStr = const Text("Deactivate User");
        break;
    }

    // Handler for submit button and "Enter" button presses in text fields
    void handleFieldSubmitted() {
      if (_formKey.currentState!.validate()) {
        switch (widget.formType) {
          case ModifyUserButtonFuntion.edit:
            ScopedModel.of<AppModel>(context).editUserInfo(_userController.text, _passController.text, _firstNameController.text,
                _lastNameController.text, _phoneNumber.phoneNumber, _emailController.text, authLevelString);
            break;
          case ModifyUserButtonFuntion.add:
            ScopedModel.of<AppModel>(context).createNewUser(_userController.text, _passController.text, _firstNameController.text,
                _lastNameController.text, _phoneController.text, _emailController.text, authLevelString);
            break;
          case ModifyUserButtonFuntion.deactivate:
            ScopedModel.of<AppModel>(context).deactivateUser(_userController.text);
            break;
        }

        Navigator.pop(context);
      }
    }

    String? validateEmail(String? value) {
      String pattern = r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]"
          r"{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]"
          r"{0,253}[a-zA-Z0-9])?)*$";
      RegExp regex = RegExp(pattern);
      if (value == null || value.isEmpty && widget.formType == ModifyUserButtonFuntion.add) {
        return 'Email is required';
      }
      if (value.isNotEmpty && !regex.hasMatch(value)) {
        return 'Enter a valid email address';
      }

      return null;
    }

// TODO: Add widget with list of users for populating fields in edit mode

    if (widget.formType != ModifyUserButtonFuntion.deactivate) {
      return Form(
        key: _formKey,
        child: AlertDialog(
          title: titleStr,
          content: SizedBox(
            width: 600,
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              const Divider(thickness: 2.0),
              // Username field
              TextFormField(
                controller: _userController,
                decoration: const InputDecoration(
                  icon: Icon(Icons.person),
                  labelText: 'Username',
                ),
                autofocus: true,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Username is required';
                  }
                  return null;
                },
                onEditingComplete: handleFieldSubmitted,
              ),
              // Password field
              TextFormField(
                controller: _passController,
                obscureText: true,
                decoration: const InputDecoration(
                  icon: Icon(Icons.password),
                  labelText: 'Password',
                ),
                validator: (value) {
                  // If formType = add then all fields are required. formType = edit allows empty fields.
                  if (value == null || value.isEmpty && widget.formType == ModifyUserButtonFuntion.add) {
                    return 'Password is required';
                  }
                  return null;
                },
                onEditingComplete: handleFieldSubmitted,
              ),
              // First Name field
              TextFormField(
                controller: _firstNameController,
                decoration: const InputDecoration(
                  icon: Icon(Icons.info),
                  labelText: 'First Name',
                ),
                validator: (value) {
                  if (value == null || value.isEmpty && widget.formType == ModifyUserButtonFuntion.add) {
                    return 'First name is required';
                  }
                  return null;
                },
                onEditingComplete: handleFieldSubmitted,
              ),
              // Last Name field
              TextFormField(
                controller: _lastNameController,
                decoration: const InputDecoration(
                  icon: Icon(Icons.info),
                  labelText: 'Last Name',
                ),
                validator: (value) {
                  if (value == null || value.isEmpty && widget.formType == ModifyUserButtonFuntion.add) {
                    return 'Last name is required';
                  }
                  return null;
                },
                onEditingComplete: handleFieldSubmitted,
              ),
              // Email field
              TextFormField(
                controller: _emailController,
                decoration: const InputDecoration(
                  icon: Icon(Icons.email),
                  labelText: 'Email Address',
                ),
                validator: (value) => validateEmail(value),
                onEditingComplete: handleFieldSubmitted,
              ),
              // Phone field
              InternationalPhoneNumberInput(
                textFieldController: _phoneController,
                selectorConfig: const SelectorConfig(
                  setSelectorButtonAsPrefixIcon: true,
                  showFlags: false,
                ),
                spaceBetweenSelectorAndTextField: 0,
                countries: const ['US'],
                maxLength: 12,
                validator: (value) {
                  if (value == null || value.isEmpty && widget.formType == ModifyUserButtonFuntion.add) {
                    return 'Phone number is required';
                  }
                  return null;
                },
                onInputChanged: (number) {
                  _phoneNumber = number;
                },
                onFieldSubmitted: (str) {
                  handleFieldSubmitted();
                },
              ),
              // AuthLevel selection
              const SizedBox(height: 20),
              const Text('Set User Authorization Level'),
              Row(
                children: [
                  Expanded(
                    child: RadioListTile(
                        value: 1,
                        title: const Text('Administrator'),
                        groupValue: authLevelRadioId,
                        onChanged: (val) {
                          setState(() {
                            authLevelRadioId = 1;
                            authLevelString = 'administrator';
                          });
                        }),
                  ),
                  Expanded(
                    child: RadioListTile(
                        value: 2,
                        title: const Text('Manager'),
                        groupValue: authLevelRadioId,
                        onChanged: (val) {
                          setState(() {
                            authLevelRadioId = 2;
                            authLevelString = 'manager';
                          });
                        }),
                  ),
                  Expanded(
                    child: RadioListTile(
                        value: 3,
                        title: const Text('User'),
                        groupValue: authLevelRadioId,
                        onChanged: (val) {
                          setState(() {
                            authLevelRadioId = 3;
                            authLevelString = 'user';
                          });
                        }),
                  ),
                ],
              ),
            ]),
          ),
          // AlertDialog submit button
          actions: <Widget>[
            TextButton(
              child: const Text('Submit'),
              onPressed: handleFieldSubmitted,
            ),
          ],
        ),
      );
    } else {
      return Form(
          key: _formKey,
          child: AlertDialog(
            title: titleStr,
            content: SizedBox(
              width: 600,
              child: Column(mainAxisSize: MainAxisSize.min, children: [
                const Divider(thickness: 2.0),
                // Username field
                TextFormField(
                  controller: _userController,
                  decoration: const InputDecoration(
                    icon: Icon(Icons.person),
                    labelText: 'Username',
                  ),
                  autofocus: true,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Username is required';
                    }
                    return null;
                  },
                  onEditingComplete: handleFieldSubmitted,
                ),
              ]),
            ),
            actions: <Widget>[
              TextButton(
                child: const Text('Submit'),
                onPressed: handleFieldSubmitted,
              ),
            ],
          ));
    }
  }
}
