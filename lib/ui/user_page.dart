import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../core/app_model.dart';
import './calendar.dart';
import './manage_employee_info_card.dart';

class UserPage extends StatelessWidget {
  const UserPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: const [
        Expanded(child: Card(child: Calendar())),
      ],
    );
  }
}
