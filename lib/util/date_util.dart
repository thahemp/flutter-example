import 'package:json_annotation/json_annotation.dart';

// This JsonConverter is used by JsonSerializable to properly handle RFC3339 dates from MongoDB
class RFC3339DateTimeConverter implements JsonConverter<DateTime, String> {
  const RFC3339DateTimeConverter();

  @override
  DateTime fromJson(String json) {
    return DateUtil.dateFromRFC3339String(json);
  }

  @override
  String toJson(DateTime json) => DateUtil.dateToRFC3339String(json);
}

class DateUtil {

  // Returns parsed DateTime object representing the supplied date.
  // If the supplied string cannot be parsed, the current date is returned.
  static DateTime dateFromRFC3339String(String utcDate) {

    var date = DateTime.tryParse(utcDate);

    if(date != null) {
      return date;
    } else {
      return DateTime.now();
    }
    
  }

  // Returns an RFC-3339 formatted string generated from a DateTime object
  static String dateToRFC3339String(DateTime dt) {

    String formatted =  "${dt.year.toString().padLeft(4, '0')}-"
                        "${dt.month.toString().padLeft(2, '0')}-"
                        "${dt.day.toString().padLeft(2, '0')}T"
                        "${dt.hour.toString().padLeft(2, '0')}:"
                        "${dt.minute.toString().padLeft(2, '0')}:"
                        "${dt.second.toString().padLeft(2, '0')}Z";

    return formatted;
    
  }

}
